<%@ page contentType="text/html;charset=UTF-8" %>
<%@ include file="/WEB-INF/views/include/taglib.jsp"%>
<html>
<head>
	<title>echarts demo管理</title>
	<meta name="decorator" content="default"/>
	<script type="text/javascript">
		$(document).ready(function() {
			
		});
	</script>

</head>
<body>
	<ul class="nav nav-tabs">
		<li class="active"><a href="${ctx}/echartsdemo/echartsdemo/bar5">echarts demo列表</a></li>
	</ul>
	<form:form id="searchForm" modelAttribute="echartsdemo" action="${ctx}/echartsdemo/echartsdemo/bar5" method="post" class="breadcrumb form-search">
		<ul class="ul-form">
			<li class="btns"><input id="btnSubmit" class="btn btn-primary" type="submit" value="查询"/></li>
			<li class="clearfix"></li>
		</ul>
	</form:form>
	
	<div id="main" style="height:400px"></div>
	<script src="${ctxStatic}/echarts/echarts.js"></script>
<script type="text/javascript">
        // 路径配置
        require.config({
            paths: {
                echarts: '${ctxStatic}/echarts'
            }
        });
        
        // 使用
        require(
            [
                'echarts',
                'echarts/chart/line', // 使用线状图就加载line模块，按需加载
                'echarts/chart/bar' // 使用柱状图就加载bar模块，按需加载
            ],
            function (ec) {
                // 基于准备好的dom，初始化echarts图表
                var myChart = ec.init(document.getElementById('main')); 
                
                var option = {
                	    title: {
                	        text: '阶梯瀑布图',
                	        subtext: 'From ExcelHome',
                	        sublink: 'http://e.weibo.com/1341556070/Aj1J2x5a5'
                	    },
                	    tooltip : {
                	        trigger: 'axis',
                	        axisPointer : {            // 坐标轴指示器，坐标轴触发有效
                	            type : 'shadow'        // 默认为直线，可选为：'line' | 'shadow'
                	        },
                	        formatter: function (params) {
                	            var tar;
                	            if (params[1].value != '-') {
                	                tar = params[1];
                	            }
                	            else {
                	                tar = params[0];
                	            }
                	            return tar.name + '<br/>' + tar.seriesName + ' : ' + tar.value;
                	        }
                	    },
                	    legend: {
                	        data:['支出','收入']
                	    },
                	    toolbox: {
                	        show : true,
                	        feature : {
                	            mark : {show: true},
                	            dataView : {show: true, readOnly: false},
                	            restore : {show: true},
                	            saveAsImage : {show: true}
                	        }
                	    },
                	    xAxis : [
                	        {
                	            type : 'category',
                	            splitLine: {show:false},
                	            data :  function (){
                	                var list = [];
                	                for (var i = 1; i <= 11; i++) {
                	                    list.push('11月' + i + '日');
                	                }
                	                return list;
                	            }()
                	        }
                	    ],
                	    yAxis : [
                	        {
                	            type : 'value'
                	        }
                	    ],
                	    series : [
                	        {
                	            name:'辅助',
                	            type:'bar',
                	            stack: '总量',
                	            itemStyle:{
                	                normal:{
                	                    barBorderColor:'rgba(0,0,0,0)',
                	                    color:'rgba(0,0,0,0)'
                	                },
                	                emphasis:{
                	                    barBorderColor:'rgba(0,0,0,0)',
                	                    color:'rgba(0,0,0,0)'
                	                }
                	            },
                	            data:[0, 900, 1245, 1530, 1376, 1376, 1511, 1689, 1856, 1495, 1292]
                	        },
                	        {
                	            name:'收入',
                	            type:'bar',
                	            stack: '总量',
                	            itemStyle : { normal: {label : {show: true, position: 'top'}}},
                	            data:[900, 345, 393, '-', '-', 135, 178, 286, '-', '-', '-']
                	        },
                	        {
                	            name:'支出',
                	            type:'bar',
                	            stack: '总量',
                	            itemStyle : { normal: {label : {show: true, position: 'bottom'}}},
                	            data:['-', '-', '-', 108, 154, '-', '-', '-', 119, 361, 203]
                	        }
                	    ]
                	};
        
                // 为echarts对象加载数据 
                myChart.setOption(option); 
            }
        );
    </script>
	
</body>
</html>